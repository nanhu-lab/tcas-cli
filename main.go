/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitee.com/nanhu-lab/tcas-cli/cmd"

func main() {
	cmd.Execute()
}
